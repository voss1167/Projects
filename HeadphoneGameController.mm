#include <iostream>
#include <string>
#include <iomanip>
#include <stdio.h>
#include <time.h>
#include <ApplicationServices/ApplicationServices.h> /* ApplicationServices.framework needed */
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <DDHidDevice/DDHidLib.h>
using namespace std;

int main (int argc, char * argv[])
{
  id array = [[DDHidDevice allDevices] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productName == \"Apple Mikey HID Driver\""]];
  DDHidDevice *mic = [array count] ? [array objectAtIndex:0] : nil;
  // it isn't a keyboard
  NSLog(@"%@", mic.primaryUsage);
  assert(mic.usage==1 && mic.usagePage==12);

}
